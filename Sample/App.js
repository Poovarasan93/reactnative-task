import React from 'react';
import { View, StyleSheet,Text} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Signup from './Signup.js';
import Loginn from './Loginn.js';
import Sign from './Sign.js';
import Icon from 'react-native-vector-icons/FontAwesome';


const Stack = createNativeStackNavigator();
 
 export default function App() {
   return (
      <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Loginn" component={Loginn} options={{headerShown:false}} />
        <Stack.Screen name="Signup" component={Signup} options={{headerShown:false}} />
        <Stack.Screen name="Sign" component={Sign} options={{headerShown:false}} />
      </Stack.Navigator>
    </NavigationContainer>
   )
 };





 